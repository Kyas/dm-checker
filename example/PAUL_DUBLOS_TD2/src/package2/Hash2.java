package fr.umlv.hash;

import java.util.Arrays;

/**
 * Class Hash2.
 * @author Jérémy Lor (jlor@etudiant.univ-mlv.fr)
 *
 */
public class Hash2 {
	private int size;
	private String[] array;

	public Hash2() {
		this(2);
	}

	// 1. Remplacer l'opération du modulo % par un masque utilisant le ET bit & 
	// bit.
	// int length = array.length - 1;
	// int index = s.hashCode()

	public Hash2(final int capacity) {
		if (capacity <= 0) {
			throw new IllegalArgumentException();
		}
		// Si ce n'est pas une puissance.
		if ((capacity & (capacity - 1)) != 0) {
			this.size = 0;
			int newCapacity = Integer.highestOneBit(capacity) << 1;
			// int newCapacity = convertToPow2(capacity);
			this.array = new String[newCapacity];
		} else {
			this.size = 0;
			this.array = new String[capacity];
		}
	}

	public void add(String s) {
		// Si le tableau est plein.
		if (size == array.length / 2) {
			array = realloc();
		}
		// La fonction géra les collisions.
		int index = index(s);
		if (index == -1) { // already exists.
			return;
		}

		array[index] = s;
		size++;
	}

	private String[] realloc() {
		int newCapacity = array.length * 2;
		String[] newArray = new String[newCapacity];
		for (int i = 0; i < array.length; i++) {
			newArray[i] = array[i];
		}
		return newArray;
	}

	private void copyAll(String[] to, String[] from) {
		for (String val : from) {
			if (val == null) {
				continue;
			}
			int index = index(val);
			if (index == -1) {
				continue;
			}
			to[index] = val;
		}
	}

	private int index(String s) {
		// Permet de rentre positif le hashcode.
		int index = (s.hashCode() & 0x7FFFFFFF) % array.length;

		// Trouver la valeur null dans les cases pour stocker b.
		for (;;) {
			// On récupère la valeur dans la case.
			String value = array[index];
			if (value == null) { // don't exist.
				return index;
			}
			if (value.equals(s)) {
				return -1;
			}
			// On passe à la case suivante alors.
			index = (index + 1) % array.length;
		}
	}

	String dump() {
		return Arrays.toString(array);
	}

	public boolean contains(String s) {
		int res = index(s);
		if (res != -1) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		String separator = "";
		for (String value : array) {
			if (value == null) {
				continue;
			}
			sb.append(separator).append(value);
			separator = ", ";
		}
		sb.append("]");

		return sb.toString();
	}

	public int convertToPow2(int n) {
		int k = Integer.highestOneBit(n);
		if (k == 0)
			return 1;
		else
			return 2 * k;
	}

	public void addAll(Hash2 hash2) {
		if (hash2.getSize() > size) {
			array = realloc();
		}
		copyAll(array, hash2.array);
	}

	public boolean intersect(Hash2 hash2) {
		if (array.length >= 1 && hash2.getArray().length >= 1) {
			for (String value : array) {
				if (value == null) {
					continue;
				}
				for (String value2 : hash2.getArray()) {
					if (value2 == null) {
						continue;
					}
					if (value.equals(value2)) {
						return true;
					}
				}
			}
		}
		return false;
	}
	// Expliquer pourquoi il est intéressant de regarder le nombre d'élements de chaque Hash2 avant d'effectuer l'algorithme
	// On s'assure qu'on a bien 1 élement dans les 2 Hashs et cela évite d'aller dans les boucles inutilement niveau complexité.

	public int getSize() {
		return size;
	}

	public String[] getArray() {
		return array;
	}

}
