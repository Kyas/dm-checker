package fr.umlv.hash;

import java.util.Arrays;

/**
 * Class Hash.
 * @author Jérémy Lor (jlor@etudiant.univ-mlv.fr)
 *
 */
public class Hash {

	// Valeur du hashcode negatif ? Il faut le rendre positif et faire un modulo
	// selon la taille N.

	// représentation binaire - puissance de 2
	// 2^n
	// <---- n ---->
	// 1 0 0 0 0 0 0 0

	// représentation binaire - puissance de 2^n-1
	// 2^n-1
	//
	// 0 1 1 1 1 1 1 1

	private int size;
	private final String[] array;

	public Hash() {
		this(2);
	}

	public Hash(final int capacity) {
		// La 2ème condition veut dire que ce n'est pas une puissance de 2.
		if (capacity <= 0 || (capacity & (capacity - 1)) != 0) {
			throw new IllegalArgumentException(
					"IllegalArgumentException : The number of elements is not a power of 2");
		}
		this.size = 0;
		this.array = new String[capacity];
	}

	public void add(String s) {
		// Si le tableau est plein.
		if (size == array.length) {
			throw new IllegalStateException();
		}
		// La fonction géra les collisions.
		int index = index(s);
		if (index == -1) { // already exists.
			return;
		}

		array[index] = s;
		size++;
	}

	private int index(String s) {
		// Permet de rentre positif le hashcode.
		int index = (s.hashCode() & 0x7FFFFFFF) % array.length;

		// Trouver la valeur null dans les cases pour stocker b.
		for (;;) {
			// On récupère la valeur dans la case.
			String value = array[index];
			if (value == null) { // don't exist.
				return index;
			}
			if (value.equals(s)) {
				return -1;
			}
			// On passe à la case suivante alors.
			index = (index + 1) % array.length;
		}
	}

	String dump() {
		return Arrays.toString(array);
	}

	public boolean contains(String s) {
		int res = index(s);
		if (res != -1) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		String separator = "";
		for (String value : array) {
			if (value == null) {
				continue;
			}
			sb.append(separator).append(value);
			separator = ", ";
		}
		sb.append("]");
		
		return sb.toString();
	}
}
