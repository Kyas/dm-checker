package fr.umlv.dmchecker.options;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import com.martiansoftware.jsap.FlaggedOption;
import com.martiansoftware.jsap.JSAP;
import com.martiansoftware.jsap.JSAPResult;
import com.martiansoftware.jsap.Parameter;
import com.martiansoftware.jsap.QualifiedSwitch;
import com.martiansoftware.jsap.UnflaggedOption;

import fr.umlv.dmchecker.model.Archives;

/**
 * OptionsFactory
 * @author Jérémy Lor (jlor@etudiant.univ-mlv.fr)
 * @author Adrien Walesch (awalesch@etudiant.univ-mlv.fr)
 * 
 */
public class OptionsFactory {

	public static Parameter[] createOptions() {
		return new Parameter[] {
				// First options ( -1 / -2 / -3 / -4 / archive)
				new QualifiedSwitch("1", JSAP.INTEGER_PARSER, JSAP.NO_DEFAULT,
						JSAP.NOT_REQUIRED, '1',
						"Requests for testing 'an' Archive."),
				new QualifiedSwitch("2", JSAP.INTEGER_PARSER, JSAP.NO_DEFAULT,
						JSAP.NOT_REQUIRED, '2',
						"Requests for testing and deploying different archives."),
				new QualifiedSwitch(
						"3",
						JSAP.INTEGER_PARSER,
						JSAP.NO_DEFAULT,
						JSAP.NOT_REQUIRED,
						'3',
						"Make junit tests on an repertoiredetest to be turned on repertoiredeprojets project."),
				new QualifiedSwitch(
						"4",
						JSAP.INTEGER_PARSER,
						JSAP.NO_DEFAULT,
						JSAP.NOT_REQUIRED,
						'4',
						"load the fichierdedonnée which contains a list of lines and evaluates the project."),
				new UnflaggedOption("archive", JSAP.STRING_PARSER,
						JSAP.NO_DEFAULT, JSAP.REQUIRED, JSAP.GREEDY,
						"One or more archives you would like to import."),

				// Second options ( -d / -v )
				new FlaggedOption("destination", JSAP.STRING_PARSER,
						JSAP.NO_DEFAULT, JSAP.NOT_REQUIRED, 'd', "destination",
						"Directory in which files are uncompressed if it isn't "
								+ "given in the second parameter."),
				new QualifiedSwitch("verbose", JSAP.STRING_PARSER,
						JSAP.NO_DEFAULT, JSAP.NOT_REQUIRED, 'v', "verbose",
						"Additional displays on System.err."),

				// Third options ( -o / -e / -x / -i / -b )
				new FlaggedOption(
						"onetop",
						JSAP.STRING_PARSER,
						JSAP.NO_DEFAULT,
						JSAP.NOT_REQUIRED,
						'o',
						"onetop",
						"One subdirectory named <onetop> in the root directory of "
								+ "the archive excluding directories and files as ignored : './'"),
				new FlaggedOption(
						"endsWith",
						JSAP.STRING_PARSER,
						JSAP.NO_DEFAULT,
						JSAP.NOT_REQUIRED,
						'e',
						"endsWith",
						"files whose name ends with <endswith> are prohibited for example: e "
								+ "~ for filenames ending with ~ or e - __ MACOSX these files "
								+ "/ directories will be ignored decompression")
						.setAllowMultipleDeclarations(true),
				new FlaggedOption(
						"exists",
						JSAP.STRING_PARSER,
						JSAP.NO_DEFAULT,
						JSAP.NOT_REQUIRED,
						'x',
						"exists",
						"Checks the file or directory (regex) the top level directory is ignored in the comparison")
						.setAllowMultipleDeclarations(true),
				new FlaggedOption(
						"interdit",
						JSAP.STRING_PARSER,
						JSAP.NO_DEFAULT,
						JSAP.NOT_REQUIRED,
						'i',
						"interdit",
						"Checks for the file or directory regex <forbidden> the top level directory is ignored in comparaison")
						.setAllowMultipleDeclarations(true),
				new FlaggedOption("beginsWith", JSAP.STRING_PARSER,
						JSAP.NO_DEFAULT, JSAP.NOT_REQUIRED, 'b', "beginsWith",
						"files / directories beginning with <startswith> are forbidden")
						.setAllowMultipleDeclarations(true),

				// Fourth options ( -O / -E / -X / -I / -B )
				new FlaggedOption(
						"forceEndsWith",
						JSAP.STRING_PARSER,
						JSAP.NO_DEFAULT,
						JSAP.NOT_REQUIRED,
						'E',
						"forceendsWith",
						"(SUDDEN DEATH) files whose name ends with <endswith> are prohibited for example: e "
								+ "~ for filenames ending with ~ or e - __ MACOSX these files "
								+ "/ directories will be ignored decompression")
						.setAllowMultipleDeclarations(true),
				new FlaggedOption(
						"forceOnetop",
						JSAP.STRING_PARSER,
						JSAP.NO_DEFAULT,
						JSAP.NOT_REQUIRED,
						'O',
						"forceOnetop",
						"(SUDDEN DEATH) One subdirectory named <onetop> in the root directory of "
								+ "the archive excluding directories and files as ignored : './'"),
				new FlaggedOption(
						"forceExists",
						JSAP.STRING_PARSER,
						JSAP.NO_DEFAULT,
						JSAP.NOT_REQUIRED,
						'X',
						"forceExists",
						"(SUDDEN DEATH) Checks the file or directory (regex) the top level directory is ignored in the comparison")
						.setAllowMultipleDeclarations(true),
				new FlaggedOption(
						"forceInterdit",
						JSAP.STRING_PARSER,
						JSAP.NO_DEFAULT,
						JSAP.NOT_REQUIRED,
						'I',
						"forceInterdit",
						"(SUDDEN DEATH) Checks for the file or directory regex <interdit> the top level directory is ignored in comparaison")
						.setAllowMultipleDeclarations(true),
				new FlaggedOption("forceBeginsWith", JSAP.STRING_PARSER,
						JSAP.NO_DEFAULT, JSAP.NOT_REQUIRED, 'B',
						"forceBeginsWith",
						"(SUDDEN DEATH) files / directories beginning with <startswith> are forbidden")
						.setAllowMultipleDeclarations(true)

		};

	}

	public static List<String> getParseOptions(JSAPResult result,
			Parameter[] params) {
		List<String> list = new LinkedList<>();
		for (int i = 0; i < params.length; i++) {
			String id = params[i].getUsageName();
			// Retrieve an option id.
			if (result.contains(id)) {
				list.add(id);
			}
		}
		return list;
	}

	public static void applyParseOptions(JSAPResult result, Parameter[] params,
			Archives archives) {
		List<String> list = getParseOptions(result, params);
		for (String o : list) {
			switch (o) {
			case "destination":
				archives.setDestination(result.getString(o));
				break;
			case "verbose":
				archives.setVerbose(result.getBoolean(o));
				break;
			case "onetop":
				archives.setOneTop(result.getString(o));
				break;
			case "exists":
				archives.getExists().addAll(
						Arrays.asList(result.getStringArray(o)));
				break;
			case "interdit":
				archives.getInterdit().addAll(
						Arrays.asList(result.getStringArray(o)));
				break;
			case "beginsWith":
				archives.getBeginsWith().addAll(
						Arrays.asList(result.getStringArray(o)));
				break;
			case "endsWith":
				archives.getEndsWith().addAll(
						Arrays.asList(result.getStringArray(o)));
				break;
			case "forceOnetop":
				archives.setForceOneTop(result.getString(o));
				break;
			case "forceExists":
				archives.getForceExists().addAll(
						Arrays.asList(result.getStringArray(o)));
				break;
			case "forceInterdit":
				archives.getForceInterdit().addAll(
						Arrays.asList(result.getStringArray(o)));
				break;
			case "forceBeginsWith":
				archives.getForceBeginsWith().addAll(
						Arrays.asList(result.getStringArray(o)));
				break;
			case "forceEndsWith":
				archives.getForceEndsWith().addAll(
						Arrays.asList(result.getStringArray(o)));
				break;
			}
		}
	}
}
