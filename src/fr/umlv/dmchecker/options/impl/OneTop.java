package fr.umlv.dmchecker.options.impl;

import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import fr.umlv.dmchecker.Messages;
import fr.umlv.dmchecker.model.Archives;
import fr.umlv.dmchecker.options.OptionsComponent;

/**
 * OneTop
 * 
 * @author Jérémy Lor (jlor@etudiant.univ-mlv.fr)
 * @author Adrien Walesch (awalesch@etudiant.univ-mlv.fr)
 * 
 */
public class OneTop extends OptionsComponent {
	private boolean isForce;

	public OneTop(final Archives archives, boolean isForce) {
		super(archives);
		this.isForce = isForce;
	}

	@Override
	public String operation() throws IOException {
		// all entries beginning by root will be returned. root represent ""
		// (zip file root) or oneTop or forceOneTop root
		ZipFile zipFile = archives.getZipFile();

		String onetop = archives.getOneTop();
		String forceOnetop = archives.getForceOneTop();

		boolean found = false;
		boolean forceFound = false;
		int moreThanOne = 0;

		if (onetop != null) {
			if (!onetop.equals("")) {
				Enumeration<? extends ZipEntry> entries = zipFile.entries();
				while (entries.hasMoreElements()) {
					ZipEntry entry = entries.nextElement();
					if (entry.getName().equals(onetop + "/")) {
						// Ignore some files
						if (entry.getName().equals("./")) {
							// Do nothing.
						}
						if (entry.isDirectory()) {
							found = true;
						}
					}

					if (!entry.getName().startsWith(onetop)
							&& entry.isDirectory()) {
						moreThanOne = 1;
					}
				}

				if (moreThanOne > 0 || !found) {
					System.out.print(Messages.getOutputString("o", onetop));
				}
			}
		} else if (forceOnetop != null) {
			if (!forceOnetop.equals("")) {
				Enumeration<? extends ZipEntry> entries = zipFile.entries();
				try {
					while (entries.hasMoreElements()) {
						ZipEntry entry = entries.nextElement();
						if (entry.getName().equals(forceOnetop + "/")) {
							if (entry.getName().equals("./")) {
								// Do nothing.
							}
							if (entry.isDirectory()) {
								forceFound = true;
							}
						}

						if (!entry.getName().startsWith(forceOnetop)
								&& entry.isDirectory()) {
							moreThanOne = 1;
						}
					}
				} catch (IllegalArgumentException e) {
					if (archives.isVerbose()) {
						System.err.println(e.getMessage());
					}
				}

				if (moreThanOne > 0 || !forceFound) {
					System.err
							.print(Messages.getOutputString("O", forceOnetop));
					return Messages.getOutputString("O", forceOnetop);
				}
			}
		}
		return "OK";
	}

	/**
	 * @return the isForce
	 */
	public boolean isForce() {
		return isForce;
	}

	@Override
	public Map<String, List<ZipEntry>> filter(List<ZipEntry> entries) {
		Map<String, List<ZipEntry>> map = new HashMap<String, List<ZipEntry>>();
		return map;
	}
}
