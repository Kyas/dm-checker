package fr.umlv.dmchecker.options.impl;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import fr.umlv.dmchecker.Messages;
import fr.umlv.dmchecker.checker.unzip.UnZipFilter;
import fr.umlv.dmchecker.checker.unzip.UnzipArchive;
import fr.umlv.dmchecker.model.Archives;
import fr.umlv.dmchecker.options.OptionsComponent;

/**
 * Destination
 * 
 * @author Jérémy Lor (jlor@etudiant.univ-mlv.fr)
 * @author Adrien Walesch (awalesch@etudiant.univ-mlv.fr)
 * 
 */
public class Destination extends OptionsComponent {
	private boolean isOption1;

	public Destination(final Archives archives, boolean option1) {
		super(archives);
		this.isOption1 = option1;
	}

	@Override
	public String operation() throws IOException {
		if (!isOption1) {
			System.out.println("Decompression In Progress...");
			String uniqueArchive = archives.getUniqueArchive();
			try {
				List<ZipEntry> filteredList = UnZipFilter
						.filterAndGenerateZipToZip(archives.getZipFile(),
								archives);
				UnzipArchive.UnzipArchiveOption2(archives.getZipFile(),
						archives.getDestination(), filteredList,
						archives.getArchives(), archives);

				System.out.println("\nDecompression Finished.");
				System.out.println(Messages.getOutputString("d",
						archives.getDestination()));

			} catch (IOException e) {
				System.err
						.println("The file is not found or unformatted ZIP (for option -2) -> "
								+ uniqueArchive + "\n");
				if (archives.isVerbose()) {
					System.err.println(e.getMessage());
				}
				return null;
			}
		} else {
			ZipFile uniqueArchive = new ZipFile(archives.getUniqueArchive());
			UnzipArchive.UnzipArchiveOption1(uniqueArchive,
					new File(archives.getDestination()));
			System.out.println(Messages.getOutputString("d",
					archives.getDestination()));
		}
		return "OK";
	}

	@Override
	public String toString() {
		return "destination";
	}

	/**
	 * @return the isOption1
	 */
	public boolean isOption1() {
		return isOption1;
	}

	@Override
	public Map<String, List<ZipEntry>> filter(List<ZipEntry> entries) {
		Map<String, List<ZipEntry>> map = new HashMap<String, List<ZipEntry>>();
		return map;
	}
}