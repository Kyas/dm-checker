package fr.umlv.dmchecker.options.impl;

import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import fr.umlv.dmchecker.Messages;
import fr.umlv.dmchecker.model.Archives;
import fr.umlv.dmchecker.options.OptionsComponent;

/**
 * Exists
 * 
 * @author Jérémy Lor (jlor@etudiant.univ-mlv.fr)
 * @author Adrien Walesch (awalesch@etudiant.univ-mlv.fr)
 * 
 */
public class Exists extends OptionsComponent {
	private boolean isForce;

	public Exists(final Archives archives, boolean isForce) {
		super(archives);
		this.isForce = isForce;
	}

	@Override
	public String operation() throws IOException {
		ZipFile zipFile = archives.getZipFile();
		// File we want to search for inside the zip file
		HashSet<String> exists = archives.getExists();
		HashSet<String> forceExists = archives.getForceExists();

		boolean found = false;
		boolean forceFound = false;
		boolean isRegexDetected = false;

		if (!isForce) {
			for (String existName : exists) {
				Enumeration<? extends ZipEntry> entries = zipFile.entries();
				while (entries.hasMoreElements()) {
					ZipEntry entry = null;
					entry = (ZipEntry) entries.nextElement();
					// Use of the regex pattern
					if (Regex.matchesPattern(entry.getName())) {
						isRegexDetected = true;
					}
					if (entry.getName().contains(existName)) {
						found = true;
					}
				}
				if (found == false) {
					System.out.print(Messages.getOutputString("x", existName));
				}
				if (!isRegexDetected) {
					System.out.print(Messages.getOutputString("i", existName));
				}
			}

		} else {
			for (String existName : forceExists) {
				Enumeration<? extends ZipEntry> entries = zipFile.entries();
				while (entries.hasMoreElements()) {
					ZipEntry entry = (ZipEntry) entries.nextElement();
					if (Regex.matchesPattern(entry.getName())) {
						isRegexDetected = true;
					}
					if (entry.getName().contains(existName)) {
						forceFound = true;
						if (archives.isVerbose()) {
							System.err.println("\t[exists] word \"" + existName
									+ "\" exists inside -> " + entry);
						}
					}
				}
				if (forceFound == false) {
					System.err.print(Messages.getOutputString("X", existName));
					return Messages.getOutputString("X", existName);
				}
				if (!isRegexDetected) {
					System.out.print(Messages.getOutputString("i", existName));
				}
			}

		}
		return "OK";
	}

	@Override
	public String toString() {
		return (isForce) ? "forceExists" : "exists";
	}

	/**
	 * @return the isForce
	 */
	public boolean isForce() {
		return isForce;
	}

	@Override
	public Map<String, List<ZipEntry>> filter(List<ZipEntry> entries) {
		Map<String, List<ZipEntry>> map = new HashMap<String, List<ZipEntry>>();
		return map;
	}

}
