package fr.umlv.dmchecker.options.impl;

import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import fr.umlv.dmchecker.Messages;
import fr.umlv.dmchecker.model.Archives;
import fr.umlv.dmchecker.options.OptionsComponent;

/**
 * BeginsWith
 * 
 * @author Jérémy Lor (jlor@etudiant.univ-mlv.fr)
 * @author Adrien Walesch (awalesch@etudiant.univ-mlv.fr)
 * 
 */
public class BeginsWith extends OptionsComponent {
	private boolean isForce;

	public BeginsWith(final Archives archives, boolean isForce) {
		super(archives);
		this.isForce = isForce;
	}

	@Override
	public String operation() throws IOException {
		ZipFile zipFile = archives.getZipFile();
		// File we want to search for inside the zip file
		HashSet<String> beginsWith = archives.getBeginsWith();
		HashSet<String> forceBeginsWith = archives.getForceBeginsWith();

		boolean found = false;
		boolean forceFound = false;

		if (!isForce) {
			for (String existName : beginsWith) {
				Enumeration<? extends ZipEntry> entries = zipFile.entries();
				while (entries.hasMoreElements()) {
					ZipEntry entry = (ZipEntry) entries.nextElement();
					String[] names = entry.getName().split("/");
					// System.out.println(names[names.length - 1]);
					if (names[names.length - 1].startsWith(existName)) {
						found = true;
						break;
					}
				}
				if (found == true) {
					System.out
							.print(Messages.getOutputString("b", existName));
				}
			}

		} else {
			for (String existName : forceBeginsWith) {
				Enumeration<? extends ZipEntry> entries = zipFile.entries();
				while (entries.hasMoreElements()) {
					ZipEntry entry = (ZipEntry) entries.nextElement();
					String[] names = entry.getName().split("/");
					if (names[names.length - 1].startsWith(existName)) {
						forceFound = true;
						if (archives.isVerbose()) {
							System.err.println("Word \""
									+ existName + "\" is forbidden inside -> "
									+ entry);
						}
						break;
					}
				}

				if (forceFound == true) {
					System.err.print(Messages.getOutputString("B", existName));
					return Messages.getOutputString("B", existName);
				}
			}

		}
		return "OK";
	}

	@Override
	public Map<String, List<ZipEntry>> filter(List<ZipEntry> entries) {
		Map<String, List<ZipEntry>> map = new HashMap<String, List<ZipEntry>>();

		if (!isForce) {
			HashSet<String> beginsWith = archives.getBeginsWith();
			// HashSet<String> forceBeginsWith = archives.getForceBeginsWith();
			List<ZipEntry> filteredList = entries.stream()
					.filter(new Predicate<ZipEntry>() {
						@Override
						public boolean test(ZipEntry z) {
							for (String s : beginsWith) {
								if (z.getName().startsWith(s)) {
									return true;
								}
							}
							return false;
						}

					}).collect(Collectors.<ZipEntry> toList());
			System.out.println(entries);
			for (ZipEntry z : filteredList) {
				System.out.println(z.getName());
			}
		}

		return map;
	}

	@Override
	public String toString() {
		return (isForce) ? "forceBeginsWith" : "beginsWith";
	}

	/**
	 * @return the isForce
	 */
	public boolean isForce() {
		return isForce;
	}

	/**
	 * @param isForce
	 *            the isForce to set
	 */
	public void setForce(boolean isForce) {
		this.isForce = isForce;
	}
}
