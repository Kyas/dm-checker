package fr.umlv.dmchecker.options.impl;

import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import fr.umlv.dmchecker.Messages;
import fr.umlv.dmchecker.model.Archives;
import fr.umlv.dmchecker.options.OptionsComponent;

/**
 * EndsWith
 * 
 * @author Jérémy Lor (jlor@etudiant.univ-mlv.fr)
 * @author Adrien Walesch (awalesch@etudiant.univ-mlv.fr)
 * 
 */
public class EndsWith extends OptionsComponent {
	private boolean isForce;

	public EndsWith(final Archives archives, boolean isForce) {
		super(archives);
		this.isForce = isForce;
	}

	@Override
	public String operation() throws IOException {
		ZipFile zipFile = archives.getZipFile();
		// File we want to search for inside the zip file
		HashSet<String> endsWith = archives.getEndsWith();
		HashSet<String> forceEndsWith = archives.getForceEndsWith();

		boolean found = false;
		boolean forceFound = false;

		if (!isForce) {
			for (String existName : endsWith) {
				Enumeration<? extends ZipEntry> entries = zipFile.entries();
				while (entries.hasMoreElements()) {
					ZipEntry entry = (ZipEntry) entries.nextElement();
					if (entry.getName().endsWith(existName)) {
						found = true;
						break;
					}
				}
				if (found == true) {
					System.out.print(Messages.getOutputString("e", existName));
				}
			}

		} else {
			for (String existName : forceEndsWith) {
				Enumeration<? extends ZipEntry> entries = zipFile.entries();
				while (entries.hasMoreElements()) {
					ZipEntry entry = (ZipEntry) entries.nextElement();
					if (entry.getName().endsWith(existName)) {
						forceFound = true;
						if (archives.isVerbose()) {
							System.err.println("Word \""
									+ existName + "\" is forbidden inside -> "
									+ entry);
						}
					}
				}

				if (forceFound == true) {
					System.err.print(Messages.getOutputString("E", existName));
					return Messages.getOutputString("E", existName);
				}
			}
		}
		return "OK";

	}

	@Override
	public String toString() {
		return (isForce) ? "forceEndsWith" : "endsWith";
	}

	/**
	 * @return the isForce
	 */
	public boolean isForce() {
		return isForce;
	}

	@Override
	public Map<String, List<ZipEntry>> filter(List<ZipEntry> entries) {
		Map<String, List<ZipEntry>> map = new HashMap<String, List<ZipEntry>>();
		return map;
	}

}
