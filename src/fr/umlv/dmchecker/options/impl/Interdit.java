package fr.umlv.dmchecker.options.impl;

import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import fr.umlv.dmchecker.Messages;
import fr.umlv.dmchecker.model.Archives;
import fr.umlv.dmchecker.options.OptionsComponent;

/**
 * Interdit
 * 
 * @author Jérémy Lor (jlor@etudiant.univ-mlv.fr)
 * @author Adrien Walesch (awalesch@etudiant.univ-mlv.fr)
 * 
 */
public class Interdit extends OptionsComponent {
	private boolean isForce;

	public Interdit(final Archives archives, boolean isForce) {
		super(archives);
		this.isForce = isForce;
	}

	@Override
	public String operation() throws IOException {
		ZipFile zipFile = archives.getZipFile();
		// File we want to search for inside the zip file
		HashSet<String> interdit = archives.getInterdit();
		HashSet<String> forceInterdit = archives.getForceInterdit();

		boolean found = false;
		boolean forceFound = false;
		boolean isRegexDetected = false;

		if (!isForce) {
			for (String existName : interdit) {
				// Use of a first Pattern to respect the roman regex.
				Enumeration<? extends ZipEntry> entries = zipFile.entries();
				while (entries.hasMoreElements()) {
					ZipEntry entry = (ZipEntry) entries.nextElement();
					if (entry.getName().contains(existName)) {
						found = true;
					}
					// if (!Charset.forName("ASCII").newEncoder()
					// .canEncode(entry.getName())) {
					// isAscii = true;
					// }

					// Use of the regex pattern
					if (Regex.matchesPattern(entry.getName())) {
						isRegexDetected = true;
					}
					if (found == true) {
						System.out.println(Messages.getOutputString("i",
								existName));
					}
				}
				if (!isRegexDetected) {
					System.out.print(Messages.getOutputString("i", existName));
				}
			}

		} else {
			for (String existName : forceInterdit) {
				Enumeration<? extends ZipEntry> entries = zipFile.entries();
				while (entries.hasMoreElements()) {
					ZipEntry entry = (ZipEntry) entries.nextElement();
					if (entry.getName().contains(existName)) {
						forceFound = true;
						if (archives.isVerbose()) {
							// Display verbose.
							System.err.println("\t[forceInterdit] word \""
									+ existName + "\" is FORBIDDEN inside -> "
									+ entry);
						}
					}
					// if (entry.getName().contains(" ")) {
					// isSpace = true;
					// }

					// Use of the regex pattern
					if (Regex.matchesPattern(entry.getName())) {
						isRegexDetected = true;
					}
				}
				if (forceFound == true) {
					System.err.print(Messages.getOutputString("I", existName));
					return Messages.getOutputString("I", existName);
				}
				if (!isRegexDetected) {
					System.err.print(Messages.getOutputString("I", existName));
				}
			}
		}
		return "OK";
	}

	@Override
	public String toString() {
		return (isForce) ? "forceInterdit" : "interdit";
	}

	/**
	 * @return the isForce
	 */
	public boolean isForce() {
		return isForce;
	}

	@Override
	public Map<String, List<ZipEntry>> filter(List<ZipEntry> entries) {
		Map<String, List<ZipEntry>> map = new HashMap<String, List<ZipEntry>>();
		return map;
	}

}
