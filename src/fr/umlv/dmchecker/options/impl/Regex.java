package fr.umlv.dmchecker.options.impl;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Regex for detecting maches in archives for options (-b/-x etc.)
 * 
 * @author Jérémy Lor (jlor@etudiant.univ-mlv.fr)
 * @author Adrien Walesch (awalesch@etudiant.univ-mlv.fr)
 * 
 */
public class Regex {

	private static String FORBIDDEN_STRING = ".*\\s.*|.*[^\\x00-\\x7F].*|~|__MACOSX";

	/**
	 * Match the sentence with the Pattern and detects.
	 * 
	 * @param sentence
	 *            The sentence.
	 * @return <code>true</code> if it matching is found, <code>false</code>
	 *         otherwise.
	 */
	public static boolean matchesPattern(String sentence) {
		Pattern p = Pattern.compile(FORBIDDEN_STRING);
		Matcher m = p.matcher(sentence);

		if (m.find()) {
			return true;
		}

		return false;
	}

}
