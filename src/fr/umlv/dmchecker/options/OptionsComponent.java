package fr.umlv.dmchecker.options;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;

import fr.umlv.dmchecker.model.Archives;

/**
 * Component.
 * 
 * @author Jérémy Lor (jlor@etudiant.univ-mlv.fr)
 * @author Adrien Walesch (awalesch@etudiant.univ-mlv.fr)
 * 
 */
public abstract class OptionsComponent {
	// Nom de "Composant"
	protected Archives archives;

	/**
	 * Constructeur
	 * 
	 * @param pNom
	 *            Nom du "Composant"
	 */
	public OptionsComponent(final Archives archives) {
		this.archives = archives;
	}

	/**
	 * Method in common to all components: Displays it is an object "Option"
	 * (like Destination) and the name they gave him.
	 * 
	 * @throws IOException
	 */
	public abstract String operation() throws IOException;

	/**
	 * Filter the zipToZip.
	 */
	public abstract Map<String, List<ZipEntry>> filter(List<ZipEntry> entries);

}
