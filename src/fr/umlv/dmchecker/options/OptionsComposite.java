package fr.umlv.dmchecker.options;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;

import fr.umlv.dmchecker.model.Archives;
import fr.umlv.dmchecker.options.impl.BeginsWith;
import fr.umlv.dmchecker.options.impl.Destination;
import fr.umlv.dmchecker.options.impl.EndsWith;
import fr.umlv.dmchecker.options.impl.Exists;
import fr.umlv.dmchecker.options.impl.Interdit;
import fr.umlv.dmchecker.options.impl.OneTop;

/**
 * OptionsComposite
 * 
 * @author Jérémy Lor (jlor@etudiant.univ-mlv.fr)
 * @author Adrien Walesch (awalesch@etudiant.univ-mlv.fr)
 * 
 */
public class OptionsComposite extends OptionsComponent {

	// OptionsComponent list for the Composite
	private List<OptionsComponent> list = new LinkedList<OptionsComponent>();

	public OptionsComposite(final Archives archives) {
		super(archives);
	}

	public void addOptionsDetected(List<String> options, boolean option1) {
		for (String o : options) {
			switch (o) {
			case "onetop":
				list.add(new OneTop(archives, false));
				break;
			case "destination":
				if (option1) {
					list.add(new Destination(archives, true));
				}
				break;
			case "exists":
				list.add(new Exists(archives, false));
				break;
			case "interdit":
				list.add(new Interdit(archives, false));
				break;
			case "beginsWith":
				list.add(new BeginsWith(archives, false));
				break;
			case "endsWith":
				list.add(new EndsWith(archives, false));
				break;
			case "forceOnetop":
				list.add(new OneTop(archives, true));
				break;
			case "forceExists":
				list.add(new Exists(archives, true));
				break;
			case "forceInterdit":
				list.add(new Interdit(archives, true));
				break;
			case "forceBeginsWith":
				list.add(new BeginsWith(archives, true));
				break;
			case "forceEndsWith":
				list.add(new EndsWith(archives, true));
				break;
			}
		}
	}

	@Override
	public String operation() throws IOException {
		final Iterator<OptionsComponent> lIterator = list.iterator();
		StringBuilder result = new StringBuilder();
		while (lIterator.hasNext()) {
			final OptionsComponent lComposant = lIterator.next();
			result.append(lComposant.operation());
		}
		return result.toString();
	}

	@Override
	public Map<String, List<ZipEntry>> filter(List<ZipEntry> entries) {
		final Iterator<OptionsComponent> lIterator = list.iterator();
		Map<String, List<ZipEntry>> map = new HashMap<String, List<ZipEntry>>(
				10);
		while (lIterator.hasNext()) {
			final OptionsComponent lComposant = lIterator.next();
			Map<String, List<ZipEntry>> m = lComposant.filter(entries);
			map.putAll(m);
		}
		return map;
	}

	public void addDestination(String destination) {
		if (archives.getDestination() != null) {
			list.remove(new Destination(archives, true));
		}
		archives.setDestination(destination);
		list.add(new Destination(archives, false));
	}

	/**
	 * Retourne la liste d'objets "Composant"
	 * 
	 * @return La liste d'objets "Composant"
	 */
	public List<OptionsComponent> getOptions() {
		return list;
	}

	/**
	 * Ajoute un objet "Composant" au "Composite"
	 * 
	 * @param pComposant
	 */
	public void add(final OptionsComponent pComposant) {
		list.add(pComposant);
	}

}
