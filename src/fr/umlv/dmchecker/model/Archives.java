package fr.umlv.dmchecker.model;

import java.util.HashSet;
import java.util.zip.ZipFile;

/**
 * Archives class.
 * @author Jérémy Lor (jlor@etudiant.univ-mlv.fr)
 * @author Adrien Walesch (awalesch@etudiant.univ-mlv.fr)
 * 
 */
public class Archives {

	private final ZipFile zipFile;
	private final String uniqueArchive;
	private final HashSet<Archive> archives;
	private String oneTop, forceOneTop, destination;
	private final HashSet<String> exists, interdit, beginsWith, endsWith,
			forceExists, forceInterdit, forceBeginsWith, forceEndsWith;
	private boolean verbose;

	public Archives(ZipFile zipFile, String uniqueArchive) {
		this.zipFile = zipFile;
		this.uniqueArchive = uniqueArchive;
		this.archives = new HashSet<>();
		this.oneTop = null;
		this.forceOneTop = null;
		this.destination = null;
		this.exists = new HashSet<>();
		this.interdit = new HashSet<>();
		this.beginsWith = new HashSet<>();
		this.endsWith = new HashSet<>();
		this.forceExists = new HashSet<>();
		this.forceInterdit = new HashSet<>();
		this.forceBeginsWith = new HashSet<>();
		this.forceEndsWith = new HashSet<>();
	}

	public Archive getArchive(String archive) {
		if (!archives.isEmpty()) {
			for (Archive a : archives) {
				if (a != null) {
					if (a.getFullname().equals(archive)) {
						return a;
					}
				}
			}
		}
		return null;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("=== ARCHIVES(s) ==\n");
		sb.append("\tName(s) -> ");
		// String[] archives = listArchives
		// .toArray(new String[listArchives.size()]);
		// int i = 0;
		// while (i < size - 1) {
		// sb.append(archives[i]).append(", ");
		// i++;
		// }
		// sb.append(archives[size - 1]);
		sb.append(uniqueArchive);
		sb.append("\n");
		sb.append("\tAttributs -> ");
		if (oneTop != null) {
			sb.append("[oneTop : " + oneTop + "] ");
		}
		if (destination != null) {
			sb.append("[destination : " + destination + "] ");
		}
		if (!exists.isEmpty()) {
			sb.append("[exists : ");
			for (String s : exists) {
				sb.append(s + " ");
			}
			sb.append("]");
		}
		if (!interdit.isEmpty()) {
			sb.append("[interdit : ");
			for (String s : interdit) {
				sb.append(s + " ");
			}
			sb.append("]");
		}
		if (!beginsWith.isEmpty()) {
			sb.append("[beginsWith : ");
			for (String s : beginsWith) {
				sb.append(s + " ");
			}
			sb.append("]");
		}
		if (!endsWith.isEmpty()) {
			sb.append("[endsWith : ");
			for (String s : endsWith) {
				sb.append(s + " ");
			}
			sb.append("]");
		}
		if (!forceExists.isEmpty()) {
			sb.append("[forceExists : ");
			for (String s : forceExists) {
				sb.append(s + " ");
			}
			sb.append("]");
		}
		if (!forceInterdit.isEmpty()) {
			sb.append("[forceInterdit : ");
			for (String s : forceInterdit) {
				sb.append(s + " ");
			}
			sb.append("]");
		}
		if (!forceBeginsWith.isEmpty()) {
			sb.append("[forceBeginsWith : ");
			for (String s : forceBeginsWith) {
				sb.append(s + " ");
			}
			sb.append("]");
		}
		if (!forceEndsWith.isEmpty()) {
			sb.append("[forceEndsWith : ");
			for (String s : forceEndsWith) {
				sb.append(s + " ");
			}
			sb.append("]");
		}
		if (verbose) {
			sb.append("[verbose]");
		}
		return sb.toString();
	}

	/**
	 * @return the oneTop
	 */
	public String getOneTop() {
		return oneTop;
	}

	/**
	 * @param oneTop
	 *            the oneTop to set
	 */
	public void setOneTop(String oneTop) {
		this.oneTop = oneTop;
	}

	/**
	 * @return the forceOneTop
	 */
	public String getForceOneTop() {
		return forceOneTop;
	}

	/**
	 * @param forceOneTop
	 *            the forceOneTop to set
	 */
	public void setForceOneTop(String forceOneTop) {
		this.forceOneTop = forceOneTop;
	}

	/**
	 * @return the destination
	 */
	public String getDestination() {
		return destination;
	}

	/**
	 * @param destination
	 *            the destination to set
	 */
	public void setDestination(String destination) {
		this.destination = destination;
	}

	/**
	 * @return the verbose
	 */
	public boolean isVerbose() {
		return verbose;
	}

	/**
	 * @param verbose
	 *            the verbose to set
	 */
	public void setVerbose(boolean verbose) {
		this.verbose = verbose;
	}

	/**
	 * @return the exists
	 */
	public HashSet<String> getExists() {
		return exists;
	}

	/**
	 * @return the interdit
	 */
	public HashSet<String> getInterdit() {
		return interdit;
	}

	/**
	 * @return the beginsWith
	 */
	public HashSet<String> getBeginsWith() {
		return beginsWith;
	}

	/**
	 * @return the endsWith
	 */
	public HashSet<String> getEndsWith() {
		return endsWith;
	}

	/**
	 * @return the forceExists
	 */
	public HashSet<String> getForceExists() {
		return forceExists;
	}

	/**
	 * @return the forceInterdit
	 */
	public HashSet<String> getForceInterdit() {
		return forceInterdit;
	}

	/**
	 * @return the forceBeginsWith
	 */
	public HashSet<String> getForceBeginsWith() {
		return forceBeginsWith;
	}

	/**
	 * @return the forceEndsWith
	 */
	public HashSet<String> getForceEndsWith() {
		return forceEndsWith;
	}

	/**
	 * @return the archives
	 */
	public HashSet<Archive> getArchives() {
		return archives;
	}

	/**
	 * @return the zipFile
	 */
	public ZipFile getZipFile() {
		return zipFile;
	}

	/**
	 * @return the uniqueArchive
	 */
	public String getUniqueArchive() {
		return uniqueArchive;
	}

}
