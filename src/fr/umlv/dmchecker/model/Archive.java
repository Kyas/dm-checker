package fr.umlv.dmchecker.model;

import java.util.zip.ZipEntry;

/**
 * Archive class.
 * @author Jérémy Lor (jlor@etudiant.univ-mlv.fr)
 * @author Adrien Walesch (awalesch@etudiant.univ-mlv.fr)
 * 
 */
public class Archive {

	private final String uniqueKey; // NOM_PRENOMS
	private final String title; // nomdonneparletudiantasonrendu
	private final int uniqueNum; // valeur numérique finale
	private final String onetop; // To keep the onetop with the unique key
	private final String fullname; // To keep the full name of the archive
	private final ZipEntry zipEntry;

	public Archive(String uniqueKey, String title, int uniqueNum,
			String fullname, String onetop, ZipEntry zipEntry) {
		this.uniqueKey = uniqueKey;
		this.title = title;
		this.uniqueNum = uniqueNum;
		this.onetop = onetop;
		this.fullname = fullname;
		this.zipEntry = zipEntry;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Archive : [").append(uniqueKey).append(", ").append(title)
				.append(", ").append(uniqueNum).append(", ").append(onetop).append("]");
		return sb.toString();
	}

	/**
	 * @return the uniqueKey
	 */
	public String getUniqueKey() {
		return uniqueKey;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @return the uniqueNum
	 */
	public int getUniqueNum() {
		return uniqueNum;
	}

	/**
	 * @return the fullname
	 */
	public String getFullname() {
		return fullname;
	}

	/**
	 * @return the onetop
	 */
	public String getOnetop() {
		return onetop;
	}

	/**
	 * @return the zipEntry
	 */
	public ZipEntry getZipEntry() {
		return zipEntry;
	}

}
