package fr.umlv.dmchecker;

/**
 * DmMessage
 * @author Jérémy Lor (jlor@etudiant.univ-mlv.fr)
 * @author Adrien Walesch (awalesch@etudiant.univ-mlv.fr)
 * 
 */
public enum Messages {
	e, o, x, i, b, E, O, X, I, B, ARCH, LISTENOTEXAUSTIVE;

	public static String getOutputString(String option, String param) {
		switch (option) {
		case "e":
		case "E":
		case "forceendsWith":
		case "endWith":
			return "File name must not end with :" + param;
		case "i":
		case "I":
		case "interdit":
		case "forceinterdit":
		case "forbidden":
		case "forceforbidden":
			return "Forbidden file name:" + param;
		case "x":
		case "X":
		case "existe":
		case "forceexiste":
			return "Missing file name :" + param;
		case "b":
		case "B":
		case "forcebeginsWith":
		case "beginsWith":
			return "File name must not start with :" + param;
		case "o":
		case "O":
		case "onetop":
		case "forceonetop":
			return "Archive with more than one top directory";
		case "d":
		case "destination":
			return "Archive has been decompressed into : " + param;
		default:
			return "UnKnown option";
		}
	}
}
