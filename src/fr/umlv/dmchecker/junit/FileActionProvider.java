package fr.umlv.dmchecker.junit;

import java.io.File;
import java.io.IOException;

/**
 * FileActionProvider Interface.
 * @author Jérémy Lor (jlor@etudiant.univ-mlv.fr)
 * @author Adrien Walesch (awalesch@etudiant.univ-mlv.fr)
 * 
 */
@FunctionalInterface
public interface FileActionProvider {

	public void perform(File file) throws IOException, InterruptedException;

}
