package fr.umlv.dmchecker.junit;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

/**
 * A usefull object that
 * <ol>
 * <li>compile source code of a project and JUnit tests</li>
 * <li>apply all JUnit test and return the result</li>
 * </ol>
 * 
 * @author JÃ©rÃ©my Lor (jlor@etudiant.univ-mlv.fr)
 * @author Adrien Walesch (awalesch@etudiant.univ-mlv.fr)
 */
public class JUnitApplier {

	private final File testDir;
	private final File sourceDir;

	/**
	 * An example that show how to use the <code>JUnitApplier</code>
	 * 
	 * @param args
	 * @throws IOException
	 * @throws InterruptedException
	 * @throws ClassNotFoundException
	 */
	public static void applyTests(String directoryTest,
			String directoryProject, String result) throws IOException,
			InterruptedException, ClassNotFoundException {
		// File testDirectory = new File(FOLDER_EXAMPLE + "/tests/");
		// File softDirectory = new File(FOLDER_EXAMPLE + "/TD2/");
		File testDirectory = new File(directoryTest);
		File softDirectory = new File(directoryProject);

		String nameStudent = directoryProject.split("/")[0];

		// applying test on software
		JUnitApplier junitApplier = new JUnitApplier(testDirectory,
				softDirectory);
		HashMap<String, Result> resultMap = junitApplier.apply();

		Writer writer = null;

		try {
			writer = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(result + ".xml"),
					"UTF-8"));

			// print example with xml form
			StringBuilder sb = new StringBuilder();
			sb.append("<dmchecker>\n");
			sb.append("<soft name=\"" + softDirectory + "\" student=\""
					+ nameStudent + "\">\n");
			for (String testName : resultMap.keySet()) {
				Result testResult = resultMap.get(testName);
				sb.append("\t<test name=\"" + testName + "\" result="
						+ testResult.wasSuccessful() + ">\n");
				if (!testResult.wasSuccessful()) {
					for (Failure failure : testResult.getFailures()) {
						sb.append("\t\t" + failure.toString() + "\n");
					}
				}
				sb.append("\t</test>\n");
			}

			// sb.append("<soft name=\"" + softDirectory + "\" student=\""
			// + nameStudent + "\">\n");
			sb.append("</soft>\n");
			// You can repeat the "<soft> </soft>" part for each software you
			// test
			// before
			// closing the "<dmchecker>" part
			sb.append("</dmchecker>\n");

			System.out.println(sb);
			writer.write(sb.toString());
		} finally {
			try {
				writer.close();
			} catch (Exception ex) {
				System.err
						.println("Something went wrong with the writing of XML result !");
			}
		}
	}

	/**
	 * Create a JUnit applier
	 * 
	 * @param testDir
	 *            the directory containing all JUnit test
	 * @param sourceDir
	 *            the Directory that contain all sources of a project
	 */
	public JUnitApplier(File testDir, File sourceDir) {
		this.testDir = testDir;
		this.sourceDir = sourceDir;
	}

	/**
	 * Apply all Junit test and return an <code>HashMap<String, Result></code>
	 * where <code>String</code> correspond to the test class name, and
	 * <code>Result</code> the result of the test
	 * 
	 * @return the HashMap where
	 *         <ul>
	 *         <li>the <i>key</i> : <code>String</code> is the test class name
	 *         <li>the <i>value</i> : <code>Result</code> is the result of the
	 *         junit test (class org.junit.runner.Result)
	 *         </ul>
	 * @throws IOException
	 * @throws InterruptedException
	 * @throws ClassNotFoundException
	 */
	public HashMap<String, Result> apply() throws IOException,
			InterruptedException, ClassNotFoundException {

		File testClassesDirectory = new File("junit_compile/tests");
		File srcClassesDirectory = new File("junit_compile/src");
		File junitJarFile = new File("lib/junit.jar");

		// Compile sources and junit test
		compileAll(sourceDir, srcClassesDirectory, srcClassesDirectory,
				junitJarFile);
		compileAll(testDir, testClassesDirectory, srcClassesDirectory,
				junitJarFile);

		// Add all URL containing classes in a single URL array
		@SuppressWarnings("deprecation")
		URL[] urls = new URL[] { testClassesDirectory.toURL(),
				srcClassesDirectory.toURL() };
		// initializing the ClassLoader with URLs containing classes
		URLClassLoader loader = new URLClassLoader(urls);
		// creating a list of test class to run
		ArrayList<String> classesToRun = new ArrayList<>();
		walkFileRecursively(testClassesDirectory, new FileActionProvider() {
			@Override
			public void perform(File f) throws IOException,
					InterruptedException {
				if (f.toString().endsWith(".class")) {
					// Here we get only the name of the file (not all the
					// path)
					// we split it using the '.class' (MyClass.class become
					// :
					// str[0]="MyClass" str[1]="")
					// and we save the first part of the splitted string,
					// that
					// will
					// be used with the ClassLoader
					classesToRun.add(f.getName().split(".class")[0]);
				}
			}
		}, (File f) -> {
		});

		// Applying all JUnit test
		HashMap<String, Result> resultMap = new HashMap<>();
		for (String className : classesToRun) {
			Class<?> testClass = loader.loadClass(className);
			resultMap.put(className, JUnitCore.runClasses(testClass));
		}
		loader.close();
		return resultMap;
	}

	/**
	 * Compile all .java in the directory <i>dir</i>
	 * 
	 * @param dir
	 *            the directory where are all .java
	 * @param destination
	 *            a directory where will be placed all .class
	 * @param srcClassDir
	 *            a directory that contain all .class needed for the compilation
	 *            (sources used in JUnit tests)
	 * @param junitJar
	 *            the junit.jar file
	 * @throws IOException
	 * @throws InterruptedException
	 */
	private void compileAll(File dir, File destination, File srcClassDir,
			File junitJar) throws IOException, InterruptedException {

		StringBuilder classPath = new StringBuilder();
		classPath.append(junitJar.getAbsolutePath());
		walkFileRecursively(srcClassDir, (File f) -> {
		}, new FileActionProvider() {
			@Override
			public void perform(File f) throws IOException,
					InterruptedException {
				classPath.append(";" + f.getAbsolutePath());
			}
		});

		walkFileRecursively(dir, new FileActionProvider() {
			@Override
			public void perform(File f) throws IOException,
					InterruptedException {
				if (f.getName().endsWith(".java")) {
					Process process = Runtime.getRuntime().exec(
							"javac " + f.getAbsolutePath() + " -cp "
									+ classPath.toString() + " -d "
									+ destination.getAbsolutePath());
					// If we don't empty the Output stream of the child process,
					// it will
					// block the execution when
					// streams are full, and we will never pass the waitFor()
					// (well it seems to pass if the cmd execution fail, but it
					// happens
					// maybe because streams are not full)
					InputStream stderr = process.getErrorStream();
					BufferedReader stderrReader = new BufferedReader(
							new InputStreamReader(stderr));
					String line = null, stderrMessage = "";
					while (null != (line = stderrReader.readLine()))
						stderrMessage += "\t" + line + "\n";
					if (0 != process.waitFor()) {
						throw new RuntimeException(
								"Error when compiling source classes :\n"
										+ stderrMessage);
					}
				}
			}
		}, (File f) -> {
		});
	}

	/**
	 * Visit recursively all files and folders in the <code>File</code> <i>f</i>
	 * and perform one of the following action:
	 * <ul>
	 * <li><i>actionOnFile</i> on files
	 * <li><i>actionOnDirectory</i> on directories
	 * </ul>
	 * the action provided by the <code>FileActionPerformer</code>
	 * 
	 * @param f
	 * @param actionOnFile
	 *            FunctionnalInterface that provide an action on a
	 *            <code>File</code> and return nothing
	 * @param actionOnFolders
	 *            FunctionnalInterface that provide an action on a
	 *            <code>File</code> and return nothing
	 * @throws IOException
	 * @throws InterruptedException
	 */
	private void walkFileRecursively(File f, FileActionProvider actionOnFile,
			FileActionProvider actionOnFolders) throws IOException,
			InterruptedException {
		if (!f.isDirectory()) {
			actionOnFile.perform(f);
		} else {
			actionOnFolders.perform(f);
			List<File> fileList = Arrays.asList(f.listFiles());
			for (File file : fileList) {
				walkFileRecursively(file, actionOnFile, actionOnFolders);
			}
		}
	}
}