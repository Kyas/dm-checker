package fr.umlv.dmchecker.checker.unzip;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

import fr.umlv.dmchecker.model.Archive;
import fr.umlv.dmchecker.model.Archives;

/**
 * UnZipArchive class.
 * 
 * @author Jérémy Lor (jlor@etudiant.univ-mlv.fr)
 * @author Adrien Walesch (awalesch@etudiant.univ-mlv.fr)
 * 
 */
public class UnzipArchive {
	/**
	 * NOTE : Les deux champs ci-dessous ont été enlevés car ces dossiers
	 * étaient par défaut, libre à l'utilisateur de choisir où il veut
	 * récupérer/extraire son zip dans la ligne de commande.
	 * 
	 * Exemple : tmp/nomduzip.zip
	 */

	// private final static String FOLDER_TMP = "tmp/";
	// private final static String OUTPUT_DESTINATION = "uncompressed_files/";

	/**
	 * Unzip archive for option 1.
	 * 
	 * @param zipFile
	 *            The zipFile.
	 * @param outputFolder
	 *            The outputFolder
	 * @throws IOException
	 */
	public static void UnzipArchiveOption1(ZipFile zipFile, File outputFolder)
			throws IOException {
		File f = null;
		FileOutputStream fos = null;
		Enumeration<? extends ZipEntry> files = zipFile.entries();

		while (files.hasMoreElements()) {
			try {
				ZipEntry entry = (ZipEntry) files.nextElement();
				InputStream eis = zipFile.getInputStream(entry);
				byte[] buffer = new byte[1024];
				int bytesRead = 0;

				f = new File(outputFolder.getAbsolutePath() + File.separator
						+ entry.getName());

				if (entry.isDirectory()) {
					f.mkdirs();
					continue;
				} else {
					f.getParentFile().mkdirs();
					f.createNewFile();
				}

				fos = new FileOutputStream(f);

				while ((bytesRead = eis.read(buffer)) != -1) {
					fos.write(buffer, 0, bytesRead);
				}
			} catch (IOException e) {
				e.printStackTrace();
				continue;
			} finally {
				if (fos != null) {
					try {
						fos.close();
					} catch (IOException e) {
						// Do Nothing here.
					}
				}
			}
		}
	}

	/**
	 * Unzip archive for option 2.
	 * 
	 * @param zipFile
	 *            The zipFile.
	 * @param outputFolder
	 *            The outputFolder
	 * @throws IOException
	 */
	public static void UnzipArchiveOption2(ZipFile zipFile,
			String outputFolder, List<ZipEntry> filteredList,
			HashSet<Archive> archivesSet, Archives archives) throws IOException {

		ZipInputStream zin = null;
		ZipInputStream zin2 = null;

		HashSet<String> beginsWith = archives.getBeginsWith();
		HashSet<String> endsWith = archives.getEndsWith();
		HashSet<String> exists = archives.getExists();
		HashSet<String> interdit = archives.getInterdit();

		String regex = null;
		StringBuilder sbRegex = new StringBuilder();
		for (String s : beginsWith) {
			sbRegex.append(s);
		}
		for (String s : endsWith) {
			sbRegex.append(s).append("|");
		}
		for (String s : exists) {
			sbRegex.append(s).append("|");
		}
		for (String s : interdit) {
			sbRegex.append(s).append("|");
		}
		regex = sbRegex.toString();

		Pattern p = Pattern.compile(regex);
		System.out.println(regex);

		for (Archive a : archivesSet) {
			File fileFullName = new File(outputFolder + "/" + a.getOnetop()
					+ a.getUniqueKey());
			fileFullName.mkdirs();

			try {
				ZipEntry z = a.getZipEntry();
				zin = new ZipInputStream(zipFile.getInputStream(z));

				// We skip the zipEntry (.zip)
				// z = zin.getNextEntry();

				ZipEntry z2 = a.getZipEntry();
				zin2 = new ZipInputStream(zipFile.getInputStream(z2));

				// We skip the zipEntry (.zip)
				// z2 = zin2.getNextEntry();

				while ((z2 = zin2.getNextEntry()) != null) {
					String fileName = z2.getName();
					Matcher m = p.matcher(fileName);
					/**
					 * FILTER - IGNORE SOME FILES HERE FOR UNZIP !
					 */
					// FIXME We detect the regex of the option (-b/-e) and we
					// ignore it but it doesn't work :/
					if (!findMatches(p, m)) {

						StringBuilder sb = new StringBuilder().append("/");
						if (!a.getOnetop().equals("")) {

							sb.append(a.getOnetop()).append("/");
						}
						sb.append(a.getUniqueKey()).append("/")
								.append(fileName);

						String pathName = sb.toString();
						File file = new File(pathName);

						File parent = file.getParentFile();
						if (parent != null) {
							parent.mkdirs();
						}

						FileOutputStream fos = new FileOutputStream(file);
						byte[] bytes = new byte[1024];
						int len;
						while ((len = zin2.read(bytes)) >= 0) {
							fos.write(bytes, 0, len);
						}

						fos.close();
					} else {
						z2 = zin2.getNextEntry();
					}

					while ((z = zin.getNextEntry()) != null) {
						String fileName2 = z.getName();
						/**
						 * FILTER - IGNORE SOME FILES HERE FOR UNZIP !
						 */
						// FIXME We detect the regex of the option (-b/-e) and
						// we ignore it but it doesn't work :/
						if (!beginsWith.contains(fileName2)
								&& !endsWith.contains(fileName2)
								&& !exists.contains(fileName2)
								&& !interdit.contains(fileName2)) {

							StringBuilder sb = new StringBuilder().append(
									outputFolder).append("/");
							if (!a.getOnetop().equals("")) {
								sb.append(a.getOnetop()).append("/");
							}
							sb.append(a.getUniqueKey()).append("/")
									.append(fileName2);

							String pathName2 = sb.toString();
							File file = new File(pathName2);

							File parent = file.getParentFile();
							if (parent != null) {
								parent.mkdirs();
							}

							FileOutputStream fos = new FileOutputStream(file);
							byte[] bytes = new byte[1024];
							int len;
							while ((len = zin.read(bytes)) >= 0) {
								fos.write(bytes, 0, len);
							}

							fos.close();
							z = zin.getNextEntry();
						} else {
							z = zin.getNextEntry();
						}
					}
				}

			} catch (IOException e) {
				System.err
						.println("Bug found here for the unzip : see UnzipArchive class.");
				System.err.println(e.getLocalizedMessage());
			} finally {
				zin.closeEntry();
				zin.close();

				zin2.closeEntry();
				zin2.close();
			}
		}

		zin.close();
	}

	private static boolean findMatches(Pattern p, Matcher m) {
		if (m.find()) {
			return true;
		}
		return false;
	}
}
