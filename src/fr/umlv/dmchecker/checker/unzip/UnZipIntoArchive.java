package fr.umlv.dmchecker.checker.unzip;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import fr.umlv.dmchecker.model.Archive;

/**
 * ZipIntoArchive - Transform an archive.
 * 
 * @author Jérémy Lor (jlor@etudiant.univ-mlv.fr)
 * @author Adrien Walesch (awalesch@etudiant.univ-mlv.fr)
 * 
 */
public class UnZipIntoArchive {
	/**
	 * Create an Archive.
	 * 
	 * @param archive
	 * @param zipEntry
	 * @param verbose
	 * @return an Archive
	 */
	public static Archive createArchive(String archive, ZipEntry zipEntry,
			boolean verbose) {
		int underscore = 0;
		String lastName = null;
		String firstName = null;
		String uniqueKey = null;
		String title = null;
		String uniqueNum = null;
		String onetop = null;

		StringBuilder sbLastName = new StringBuilder();
		StringBuilder sbFirstName = new StringBuilder();
		StringBuilder sbConcatName = new StringBuilder();
		StringBuilder sbTitle = new StringBuilder();
		StringBuilder sbUniqueNum = new StringBuilder();
		StringBuilder sbOnetop = new StringBuilder();

		if (archive.contains("/")) {
			String[] tokens = archive.split("/");
			// Retrieve just the zipname.
			archive = tokens[tokens.length - 1];
			for (int i = 0; i < tokens.length - 1; i++) {
				sbOnetop.append(tokens[i]).append("/");
			}
			onetop = sbOnetop.toString();
		} else {
			onetop = "";
		}

		for (int i = 0; i < archive.length(); i++) {
			char c = archive.charAt(i);

			// First, we retrieve NOMS
			switch (underscore) {
			case 0:
				if (c != '_') {
					sbLastName.append(Character.toUpperCase(c));
				} else {
					underscore++;
				}
				break;
			case 1:
				if (c != '_') {
					sbFirstName.append(Character.toUpperCase(c));
				} else {
					underscore++;
				}

				break;
			case 2:
				// Example : _112
				if (c == '_' && Character.isDigit(archive.charAt(i + 1))) {
					underscore++;
				} else {
					sbTitle.append(c);
				}

				break;
			case 3:
				if (Character.isDigit(c)) {
					sbUniqueNum.append(c);
				}
				break;
			}
		}
		lastName = sbLastName.toString();
		firstName = sbFirstName.toString();

		// Concatenation NOM_PRENOMS
		uniqueKey = sbConcatName.append(lastName).append("_").append(firstName)
				.toString();
		title = sbTitle.toString();
		uniqueNum = sbUniqueNum.toString();

		// Debug
		System.out.println();
		System.out.println("\tUnique Key : " + uniqueKey);
		System.out.println("\tTitle : " + title);
		System.out.println("\tUnique Num : " + uniqueNum);
		System.out.println("\tOnetop : " + onetop);

		return new Archive(uniqueKey, title, Integer.valueOf(uniqueNum),
				archive, onetop, zipEntry);
	}

	public static boolean renameCorrectZips(Path path, boolean verbose) {
		if (path == null) {
			return false;
		}
		boolean isZipInside = false;
		boolean containsUnderscore = false;
		boolean containsSpace = false;
		// get a temp file
		File zipFile = path.toFile();
		// Create temp file.
		File temp = null;
		try {
			temp = File.createTempFile("temp", ".zip", new File("."));
		} catch (IOException e) {
			System.err.println("Creation temp failed.");
			if (verbose) {
				System.err.println(e.getLocalizedMessage());
			}
			return false;
		}

		byte[] buf = new byte[4096 * 1024];

		ZipInputStream zin = null;
		ZipOutputStream out = null;
		try {
			zin = new ZipInputStream(new FileInputStream(zipFile));
			out = new ZipOutputStream(new FileOutputStream(temp));

			ZipEntry entry;
			entry = zin.getNextEntry();
			while (entry != null) {
				String name = entry.getName();
				String newName = name;
				boolean replace = false;
				if (name.endsWith(".zip")) {
					isZipInside = true;
					// Rename
					for (int i = 0; i < name.length(); i++) {
						char c = name.charAt(i);
						if (c == ' ') {
							newName = name.replace(c, '_');
							replace = true;
							containsSpace = true;
						}
						if (c == '_') {
							containsUnderscore = true;
						}
					}
					if (replace) {
						// Display verbose.
						if (verbose) {
							System.err.println("The file " + name
									+ " was invalid. It was renamed by : "
									+ newName);
						}
					}
				}
				// Add ZIP entry to output stream.
				out.putNextEntry(new ZipEntry(newName));
				// Transfer bytes from the ZIP file to the output file
				int len;
				while ((len = zin.read(buf)) > 0) {
					out.write(buf, 0, len);
				}
				entry = zin.getNextEntry();
			}
			if (!isZipInside) {
				if (verbose) {
					System.err
							.println("The ZIP to zip File doesn't contains zip file(s) !");
				}
				return false;
			}

			if (!containsSpace && !containsUnderscore) {
				if (verbose) {
					System.err
							.println("The ZIP to zip File doesn't contains spaces or underscores !");
				}
				return false;
			} else {
				System.out
						.println("The ZIP to zip : "
								+ path.getFileName()
								+ " contains spaces inside, they'll be renamed.");
			}
		} catch (FileNotFoundException e) {
			System.err.println("File not found : " + zipFile);
			return false;
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				// Close the streams
				zin.close();
				// Complete the entry
				out.closeEntry();
				// Finish.
				out.finish();
				// Complete the ZIP file
				out.close();
			} catch (IOException e) {
				System.err
						.println("[ERROR] Close the stream for the transformation.");
				return false;
			}
		}

		// Delete the temporary file.
		temp.deleteOnExit();

		return true;

	}

}
