package fr.umlv.dmchecker.checker.unzip;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import fr.umlv.dmchecker.model.Archive;
import fr.umlv.dmchecker.model.Archives;

/**
 * ZipFilter - Zip the actual zip
 * 
 * @author Jérémy Lor (jlor@etudiant.univ-mlv.fr)
 * @author Adrien Walesch (awalesch@etudiant.univ-mlv.fr)
 * 
 */
public class UnZipFilter {

	/**
	 * Retrieve all entries from the zipFile to a list.
	 * 
	 * @param zipFile
	 *            The zipFile
	 * @return a list.
	 */
	public static List<ZipEntry> zipToList(ZipFile zipFile) {
		Enumeration<? extends ZipEntry> enu = zipFile.entries();
		List<ZipEntry> list = new ArrayList<>();
		while (enu.hasMoreElements()) {
			ZipEntry entry = enu.nextElement();
			list.add(entry);
		}
		return list;
	}

	/**
	 * Filter the actual zip with some terms and generate an object "Archive"
	 * for each archive. Returns a list of filtered ZipEntries.
	 * 
	 * @param zipFile
	 *            The zipFile
	 * @param archives
	 *            The archives
	 * @return The List of ZipEntry
	 */
	public static List<ZipEntry> filterAndGenerateZipToZip(ZipFile zipFile,
			Archives archives) {
		Enumeration<? extends ZipEntry> enu = zipFile.entries();
		List<ZipEntry> list = new ArrayList<>();
		while (enu.hasMoreElements()) {
			ZipEntry entry = enu.nextElement();
			list.add(entry);
		}

		List<ZipEntry> filteredList = list.stream()
				.filter(z -> z.getName().endsWith(".zip"))
				.collect(Collectors.<ZipEntry> toList());

		// Creation of an Archive for each.
		HashSet<Archive> archivesSet = archives.getArchives();
		System.out.println("Using of a filter which retrieve .zip files");
		System.out.println("Rename incorrect zips if necessary.");
		System.out.println("Creation of Archives Detected.");
		for (ZipEntry z : filteredList) {
			String newZipName = renameCorrectZipName(z.getName());
			Archive a = UnZipIntoArchive.createArchive(newZipName, z,
					archives.isVerbose());
			archivesSet.add(a);
		}

		// Debug.
		// System.out.println();
		// for (Archive a : archivesSet) {
		// System.out.println(a);
		// }

		return filteredList;
	}

	/**
	 * 
	 * @param name
	 * @return
	 */
	private static String renameCorrectZipName(String name) {
		if (name.contains(" ")) {
			return name.replaceAll(" ", "_");
		}
		if (!Charset.forName("ASCII").newEncoder().canEncode(name)) {
			System.err.println("There is a file/folder which contains ASCII !");
		}
		return name;
	}
}
