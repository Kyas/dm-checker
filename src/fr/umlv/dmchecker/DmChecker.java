package fr.umlv.dmchecker;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.List;
import java.util.zip.ZipFile;

import com.martiansoftware.jsap.JSAPException;
import com.martiansoftware.jsap.JSAPResult;
import com.martiansoftware.jsap.Parameter;
import com.martiansoftware.jsap.SimpleJSAP;

import fr.umlv.dmchecker.checker.unzip.UnZipIntoArchive;
import fr.umlv.dmchecker.junit.JUnitApplier;
import fr.umlv.dmchecker.model.Archives;
import fr.umlv.dmchecker.options.OptionsComposite;
import fr.umlv.dmchecker.options.OptionsFactory;

/**
 * 
 * @author Jérémy Lor (jlor@etudiant.univ-mlv.fr)
 * @author Adrien Walesch (awalesch@etudiant.univ-mlv.fr)
 * 
 */
public class DmChecker {
	public static Charset US_ASCII = StandardCharsets.US_ASCII;

	public static void main(String[] args) throws JSAPException, IOException {
		// displayTitle();

		// We create the Parameter[] for all options.
		Parameter[] params = OptionsFactory.createOptions();

		SimpleJSAP jsap = new SimpleJSAP("DmChecker",
				"Import one or more archives", params);

		// We parse the JSAP command line.
		JSAPResult result = jsap.parse(args);
		if (jsap.messagePrinted()) {
			System.exit(1);
		}
		/**
		 * Options.
		 */
		List<String> list = OptionsFactory.getParseOptions(result, params);
		// System.out.print("OPTIONS DETECTED : ");
		// System.out.println(list);

		boolean isOption1 = result.contains("1");
		boolean isOption2 = result.contains("2");
		boolean isOption3 = result.contains("3");
		boolean isOption4 = result.contains("4");

		if (!isOption1 && !isOption2 && !isOption3 && !isOption4) {
			System.err.println("Error ! No options detected");
			// No needed custom help because jsap gives this.
			System.err.println(jsap.getHelp());
			System.exit(1);
		}

		// Adding of archive(s) into a listArchives.
		String[] archivesArray = result.getStringArray("archive");
		HashSet<String> listArchives = new HashSet<>();

		// Depends of the size (for option -1 or option -2)
		if (archivesArray.length == 1) {
			listArchives.add(archivesArray[0]);
		}

		Archives archives = null;
		String destination = null;
		if (!isOption3) {
			String uniqueArchive = archivesArray[0];
			ZipFile zipFile = new ZipFile(uniqueArchive, US_ASCII);
			archives = new Archives(zipFile, uniqueArchive);

			// Apply options DETECTED here (with values)
			OptionsFactory.applyParseOptions(result, params, archives);

			/**
			 * Traitement of the archive.
			 */
			// System.out.println("Treatment of the unique archive : "
			// + archivesArray[0]);
		}

		// MODE -1
		if (isOption1) {
			// Use of Composite Design Pattern (to call the operation
			// detected with specific options)
			final OptionsComposite composite = new OptionsComposite(archives);
			composite.addOptionsDetected(list, true);
			if (archivesArray.length > 1) {
				// Error if there are more than one archive.
				System.err.println("You can't put 2 archives with option -1 !");
				// Exit the program.
				System.exit(1);
			}

			try {
				String res = composite.operation();
				if (res.equals("OK")) {
					System.exit(0);
				} else {
					System.exit(1);
				}
			} catch (IllegalArgumentException e) {
				// Display verbose.
				System.out.println("The ZIP is Malformed");
				System.exit(1);
				if (archives.isVerbose()) {
					System.err.println(e.getMessage());
				}
			}
			System.exit(0);
		}

		// MODE -2
		if (isOption2) {
			// Use of Composite Design Pattern (to call the operation
			// detected with specific options)
			final OptionsComposite composite = new OptionsComposite(archives);
			composite.addOptionsDetected(list, true);
			if (archivesArray.length != 2) {
				// Error if there are more than one archive.
				System.err.println("You can't put 2 archives with option -2 !");
				// Exit the program.
				System.exit(1);
			}
			destination = archivesArray[1];
			composite.addDestination(destination);

			/**
			 * Parse the current Zip to Zip to see if a zip contains spaces and
			 * then correct it to a correct format
			 */
			String pathUniqueArchive = archives.getUniqueArchive();
			if (!UnZipIntoArchive.renameCorrectZips(
					Paths.get(pathUniqueArchive), archives.isVerbose())) {
				System.err
						.println("Something went wrong with the file - Unformat ZIP ?");
				System.exit(1);
			}

			try {
				String res = composite.operation();
				if (res.equals("OK")) {
					System.exit(0);
				} else {
					System.exit(1);
				}
			} catch (IllegalArgumentException e) {
				// Display verbose.
				System.out.println("The ZIP is Malformed");
				System.err.println(e.getLocalizedMessage());
				System.exit(1);
				if (archives.isVerbose()) {
					System.err.println(e.getMessage());
				}
			} catch (IOException e) {
				System.out.println("Something went wrong with the file");
				System.exit(1);
				// Display verbose.
				if (archives.isVerbose()) {
					System.err.println(e.getMessage());
				}
			}
			System.exit(0);
		}

		// MODE -3
		if (isOption3) {
			if (archivesArray.length != 3) {
				// Error if there are more than one archive.
				System.err
						.println("You need to put 3 arguments (testDirectory,projectDirectory,result) with option -3 !");
				// Exit the program.
				System.exit(1);
			}
			String testDirectory = archivesArray[0];
			String projectDirectory = archivesArray[1];
			String resultDirectory = archivesArray[2];
			try {
				JUnitApplier.applyTests(testDirectory, projectDirectory,
						resultDirectory);
			} catch (ClassNotFoundException e) {
				System.err.println("Something went wrong with classes !");
			} catch (InterruptedException e) {
				System.err.println("Something went wrong ! Interrupted !");
			}
		}

		// MODE -4
		if (isOption4) {
			System.out.println("\n#### OPTION 4 ####");
			// No time to do it sorry :(
		}

	}
}
