package fr.umlv.dmchecker;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.FileSystems;
import java.security.Permission;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.junit.Before;
import org.junit.Test;

/**
 * Created with IntelliJ IDEA. User: dr Date: 11/28/13 Time: 4:56 PM To change
 * this template use File | Settings | File Templates.
 */
public class TestComplet {
	// private final static PrintStream oo = System.out;
	// private final static PrintStream ee = System.err;
	private String[] splo;
	private String[] sple;

	static class UnrollError extends Error {
		/**
		 * 
		 */
		private static final long serialVersionUID = -1397490840047304351L;
		final int exitCode;

		UnrollError(int exitCode) {
			this.exitCode = exitCode;
		}
	}

	static {
		System.setSecurityManager(new SecurityManager() {
			@Override
			public void checkPermission(Permission perm) {
			}

			@Override
			public void checkPermission(Permission perm, Object context) {
			}

			@Override
			public void checkExit(int status) {
				throw new UnrollError(status);
			}
		});
	}

	private ByteArrayOutputStream bo;
	private ByteArrayOutputStream be;
	private String repertoire = "tmp/";

	@Before
	public void setup() {
		bo = new ByteArrayOutputStream(10000);
		System.setOut(new PrintStream(bo));
		be = new ByteArrayOutputStream(10000);
		System.setErr(new PrintStream(be));

		try (ZipOutputStream zout = new ZipOutputStream(new FileOutputStream(
				repertoire + "/notop.zip"))) {
			zout.putNextEntry(new ZipEntry("fichierOhTop"));
			zout.write(65);
			zout.write(65);
			zout.write(65);
			zout.write(65);
			zout.closeEntry();
			zout.putNextEntry(new ZipEntry("autrefichierautop"));
			zout.write(65);
			zout.write(65);
			zout.write(65);
			zout.write(65);
			zout.closeEntry();
		} catch (IOException e) {
			System.out.println(e.getLocalizedMessage());
			fail("problemes de creation des fichiers de test dans tmp "
					+ e.getLocalizedMessage());
		}

		assertTrue(java.nio.file.Files.isReadable(FileSystems.getDefault()
				.getPath(repertoire, "notop.zip")));

		assertTrue(java.nio.file.Files.isReadable(FileSystems.getDefault()
				.getPath(repertoire, "zip2zip.zip")));

		assertTrue(java.nio.file.Files.isReadable(FileSystems.getDefault()
				.getPath(repertoire, "unprojet.zip")));

	}

	private boolean find(String s, String[] tab) {
		for (String d : tab) {
			if (s.equals(d))
				return true;
		}
		return false;
	}

	@Test
	public void testEtoileExiste() throws Exception {
		final String existename = ".*\\s.*"; // pas de espace dans les noms de
												// fichier
		final String ascii = ".*[^\\x00-\\x7F].*"; // pas de caractère non ascii
		try {
			String[] args = new String[] { "-1", "-I", existename, "-i", ascii,
					repertoire + "/unprojet.zip" };

			fr.umlv.dmchecker.DmChecker.main(args);
			fail("no exit");
		} catch (UnrollError e) {
			org.junit.Assert.assertEquals(1, e.exitCode);
			String[] splo = bo.toString().split("\n");
			String[] sple = be.toString().split("\n");

			assertTrue("pas de space dans les noms de fichiers ",
					find(Messages.getOutputString("I", existename), sple));
			assertTrue("pas de caractère nom ascii dans les noms de fichiers"
					+ be + "\n" + Messages.getOutputString("i", ascii),
					find(Messages.getOutputString("i", ascii), splo));
		}
	}

	public void readBuffers() {
		splo = bo.toString().split("\n");
		sple = be.toString().split("\n");
	}

	@Test
	public void testendWith1() throws Exception {
		final String existename = "findemot";
		final String option = "e";
		try {
			String[] args = new String[] { "-1", "-" + option, existename,
					repertoire + "/unprojet.zip" };

			fr.umlv.dmchecker.DmChecker.main(args);
			fail("no exit");
		} catch (UnrollError e) {
			org.junit.Assert.assertEquals(0, e.exitCode);
			readBuffers();
			assertEquals(Messages.getOutputString("e", existename), splo[0]);
		}
	}

	@Test
	public void testnotop() throws Exception {
		final String existename = "findemot";
		final String option = "o";
		try {
			String[] args = new String[] { "-1", "-" + option, existename,
					repertoire + "/notop.zip" };

			fr.umlv.dmchecker.DmChecker.main(args);
			fail("no exit");
		} catch (UnrollError e) {
			org.junit.Assert.assertEquals(0, e.exitCode);
			readBuffers();
			assertEquals(Messages.getOutputString("o", existename), splo[0]);
		}
	}

	@Test
	public void testendWithforce() throws Exception {
		final String existename = "findemot";
		final String option = "E";
		try {
			String[] args = new String[] { "-1", "-" + option, existename,
					repertoire + "/unprojet.zip" };

			fr.umlv.dmchecker.DmChecker.main(args);
			fail("no exit");
		} catch (UnrollError e) {
			org.junit.Assert.assertEquals(1, e.exitCode);
			readBuffers();
			assertEquals(Messages.getOutputString("E", existename), sple[0]);
		}
	}

	@Test
	public void testbegin() throws Exception {
		final String existename = "begin";
		final String option = "b";
		try {
			String[] args = new String[] { "-1", "-" + option, existename,
					repertoire + "/unprojet.zip" };

			fr.umlv.dmchecker.DmChecker.main(args);
			fail("no exit");
		} catch (UnrollError e) {
			org.junit.Assert.assertEquals(0, e.exitCode);
			readBuffers();
			assertEquals(Messages.getOutputString("b", existename), splo[0]);
		}
	}

	@Test
	public void testbeginforce() throws Exception {
		final String existename = "begin";
		final String option = "B";
		try {
			String[] args = new String[] { "-1", "-" + option, existename,
					repertoire + "/unprojet.zip" };

			fr.umlv.dmchecker.DmChecker.main(args);
			fail("no exit");
		} catch (UnrollError e) {
			org.junit.Assert.assertEquals(1, e.exitCode);
			readBuffers();
			assertEquals(Messages.getOutputString("B", existename), sple[0]);
		}
	}

	@Test
	public void testx() throws Exception {
		final String existename = "bob";
		final String option = "x";
		try {
			String[] args = new String[] { "-1", "-" + option, existename,
					repertoire + "/notop.zip" };

			DmChecker.main(args);
			fail("no exit");
		} catch (UnrollError e) {
			assertEquals(0, e.exitCode);
			readBuffers();
			assertEquals(Messages.getOutputString(option, existename), splo[0]);

		}
	}

	@Test
	public void testxwithdir() throws Exception {
		final String existename = "bob/begin";
		final String option = "x";
		try {
			String[] args = new String[] { "-1", "-" + option, existename,
					repertoire + "/notop.zip" };

			DmChecker.main(args);
			fail("no exit");
		} catch (UnrollError e) {
			assertEquals(Character.isLowerCase(option.charAt(0)) ? 0 : 1,
					e.exitCode);
			readBuffers();
			assertEquals(Messages.getOutputString(option, existename), splo[0]);

		}
	}

}
